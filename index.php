<?php

    require_once('animal.php');
    $animal = new Animal("Shaun");
    echo "Name =" . $animal->name . "<br>";
    echo "Legs =" . $animal->legs . "<br>";
    echo "Cold Blooded =" . $animal->cold_blooded . "<br> <br>";

    require_once('kodok.php');
    $kodok = new Kodok("Buduk");
    echo "Name =" . $kodok->name . "<br>";
    echo "Legs =" . $kodok->legs . "<br>";
    echo "Cold Blooded =" . $kodok->cold_blooded . "<br>";
    echo "Jump =" . $kodok->jump . "<br> <br>";

    require_once('sungokong.php');
    $sungokong = new Ape ("Kera Sakti");
    echo "Name =" . $sungokong->name . "<br>";
    echo "Legs =" . $sungokong->legs . "<br>";
    echo "Cold Blooded =" . $sungokong->cold_blooded . "<br>";
    echo "Yell =" . $sungokong->yell . "<br>";
?> 